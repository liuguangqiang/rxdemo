package com.liuguangqiang.rxdemo;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;

/**
 * Created by Eric on 2017/7/18.
 */
public class CombineLatest {

  public static void main(String[] args) {
    Observable<Integer> observable1 = Observable.just(1);
    Observable<Integer> observable2 = Observable.just(2);

    Observable.combineLatest(observable1, observable2, new BiFunction<Integer, Integer, Integer>() {
      public Integer apply(Integer integer, Integer integer2) throws Exception {
        return integer + integer2;
      }
    }).subscribe(new Observer<Integer>() {
      public void onSubscribe(Disposable disposable) {

      }

      public void onNext(Integer integer) {
        System.out.println("CombineLatest :" + integer);
      }

      public void onError(Throwable throwable) {

      }

      public void onComplete() {

      }
    });
  }

}
