package com.liuguangqiang.rxdemo;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;

/**
 * Created by Eric on 2017/7/18.
 */
public class Scan {

  public static void main(String[] args) {
    Observable.just(1, 2, 3, 4, 5).scan(new BiFunction<Integer, Integer, Integer>() {
      public Integer apply(Integer sum, Integer i) throws Exception {
        return sum + i;
      }
    }).subscribe(new Observer<Integer>() {
      public void onSubscribe(Disposable disposable) {

      }

      public void onNext(Integer integer) {
        System.out.println("onNext:" + integer);
      }

      public void onError(Throwable throwable) {

      }

      public void onComplete() {

      }
    });
  }

}
