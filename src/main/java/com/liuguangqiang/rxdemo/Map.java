package com.liuguangqiang.rxdemo;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;

/**
 * Created by Eric on 2017/7/18.
 */
public class Map {

  public static void mapWithLambda() {
    System.out.println("----mapWithLambda-----");
    Observable.create(observableEmitter -> observableEmitter.onNext(1))
        .map(i -> "Map to" + i)
        .subscribe(s -> System.out.println(s));
  }

  public static void map() {
    System.out.println("------map-----");
    Observable.create(new ObservableOnSubscribe<Integer>() {
      public void subscribe(ObservableEmitter<Integer> observableEmitter) throws Exception {
        observableEmitter.onNext(1);
        observableEmitter.onNext(2);
      }
    }).map(new Function<Integer, String>() {
      public String apply(Integer integer) throws Exception {
        return "Map to " + integer;
      }
    }).subscribe(new Observer<String>() {
      public void onSubscribe(Disposable disposable) {

      }

      public void onNext(String s) {
        System.out.println(s);
      }

      public void onError(Throwable throwable) {

      }

      public void onComplete() {

      }
    });
  }

  public static void main(String[] args) {
    map();
    mapWithLambda();
  }

}
