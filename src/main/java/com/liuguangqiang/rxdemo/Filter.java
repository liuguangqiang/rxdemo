package com.liuguangqiang.rxdemo;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Predicate;

/**
 * Created by Eric on 2017/7/18.
 */
public class Filter {

  public static void filterWithLambda() {
    System.out.println("filterWithLambda");
    Observable.just(1, 3, 7, 9)
        .filter(integer -> integer < 9)
        .subscribe(integer -> System.out.println("onNext:" + integer));
  }

  public static void main(String[] args) {
    Observable.just(1, 3, 7, 9).filter(new Predicate<Integer>() {
      public boolean test(Integer integer) throws Exception {
        return integer < 9;
      }
    }).subscribe(new Observer<Integer>() {
      public void onSubscribe(Disposable disposable) {

      }

      public void onNext(Integer integer) {
        System.out.println("onNext:" + integer);
      }

      public void onError(Throwable throwable) {

      }

      public void onComplete() {
        System.out.println("onComplete");
      }
    });

    filterWithLambda();
  }

}
